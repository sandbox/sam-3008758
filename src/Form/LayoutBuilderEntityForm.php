<?php

namespace Drupal\layout_builder_entity_form\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Form\FormStateInterface;

/**
 * An entity form for serving layout builder pages.
 */
class LayoutBuilderEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function getFormDisplay(FormStateInterface $form_state) {
    // Create a transient display that is not persisted, but used only for
    // building the components required for the layout form.
    $display = EntityFormDisplay::create([
      'targetEntityType' => $this->entity->getEntityTypeId(),
      'bundle' => $this->entity->bundle(),
    ]);
    $display->setComponent('layout_builder__layout', [
      'type' => 'layout_builder_widget',
      'weight' => 1,
      'settings' => [],
    ]);
    if ($log_message_field = $this->entity->getEntityType()->getRevisionMetadataKey('revision_log_message')) {
      $display->setComponent($log_message_field, [
        'weight' => 2,
        'type' => 'string_textarea',
        'settings' => [
          'rows' => 2,
        ],
      ]);
    }

    // Allow modules to choose if they are relevant to the layout builder entity
    // form.
    $this->moduleHandler->alter('layout_builder_entity_form_display', $display);
    return $display;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    // Add a bit of spacing.
    $form['spacing'] = ['#markup' => '<br/>', '#weight' => -1000];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->messenger()->addStatus($this->t('Layout has been saved.'));
    $form_state->setRedirectUrl($this->entity->toUrl($this->entity->isDefaultRevision() ? 'canonical' : 'latest-version'));
  }

}
