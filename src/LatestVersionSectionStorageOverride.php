<?php

namespace Drupal\layout_builder_entity_form;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;

/**
 * Override the section storage plugin.
 */
class LatestVersionSectionStorageOverride extends OverridesSectionStorage {

  /**
   * {@inheritdoc}
   */
  public function getSectionListFromId($id) {
    if (strpos($id, '.') !== FALSE) {
      list($entity_type_id, $entity_id) = explode('.', $id, 2);

      // Load the latest revision instead of the default. @todo, core should
      // provide this. https://www.drupal.org/project/drupal/issues/2942907.
      $storage = $this->entityTypeManager->getStorage($entity_type_id);
      $entity = $storage->loadRevision($storage->getLatestRevisionId($entity_id));

      if ($entity instanceof FieldableEntityInterface && $entity->hasField('layout_builder__layout')) {
        return $entity->get('layout_builder__layout');
      }
    }

    throw new \InvalidArgumentException(sprintf('The "%s" ID for the "%s" section storage type is invalid', $id, $this->getStorageType()));
  }

}
