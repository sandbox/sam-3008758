<?php

namespace Drupal\layout_builder_entity_form\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\layout_builder\Plugin\SectionStorage\SectionStorageLocalTaskProviderInterface;
use Drupal\layout_builder\SectionStorage\SectionStorageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides local task definitions for the layout builder entity forms.
 */
class LayoutBuilderEntityFormLocalTaskDeriver extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The section storage manager.
   *
   * @var \Drupal\layout_builder\SectionStorage\SectionStorageManagerInterface
   */
  protected $sectionStorageManager;

  /**
   * Constructs a new LayoutBuilderLocalTaskDeriver.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\layout_builder\SectionStorage\SectionStorageManagerInterface $section_storage_manager
   *   The section storage manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, SectionStorageManagerInterface $section_storage_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->sectionStorageManager = $section_storage_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.layout_builder.section_storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->sectionStorageManager->getDefinitions() as $plugin_id => $definition) {

      $section_storage = $this->sectionStorageManager->loadEmpty($plugin_id);
      if (!$section_storage instanceof SectionStorageLocalTaskProviderInterface) {
        continue;
      }

      $base_local_tasks = $section_storage->buildLocalTasks($base_plugin_definition);
      foreach ($base_local_tasks as $task_id => $original_local_task) {

        // Insert a local task next to the original one.
        if (\preg_match('/layout_builder\.overrides\.(?<entity_type_id>[a-z_]+)\.view/', $task_id, $task_information)) {
          $entity_type_id = $task_information['entity_type_id'];
          $this->derivatives["layout_builder.overrides.$entity_type_id.view_entity_form"] = $base_plugin_definition + [
            'route_name' => sprintf('%s.entity_form', $original_local_task['route_name']),
            'weight' => $original_local_task['weight'] + 1,
            'title' => $this->t('Layout (entity form)'),
            'base_route' => "entity.$entity_type_id.canonical",
          ];
        }
      }
    }

    return $this->derivatives;
  }

}
