<?php

namespace Drupal\layout_builder_entity_form\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Controller\LayoutBuilderController;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;

/**
 * A widget to display the layout form.
 *
 * @FieldWidget(
 *   id = "layout_builder_widget",
 *   label = @Translation("Layout Builder Widget"),
 *   description = @Translation("A field widget for layout builder."),
 *   field_types = {
 *     "layout_section"
 *   },
 *   multiple_values = TRUE
 * )
 */
class LayoutBuilderWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $entity = $items->getEntity();

    /** @var \Drupal\layout_builder\SectionStorage\SectionStorageManager $section_storage_manager */
    /** @var \Drupal\layout_builder\LayoutTempstoreRepository $temp_store */
    $section_storage_manager = \Drupal::service('plugin.manager.layout_builder.section_storage');

    $section_storage = $section_storage_manager->loadFromStorageId('overrides', sprintf('%s.%s', $entity->getEntityTypeId(), $entity->id()));

    /** @var \Drupal\layout_builder\Controller\LayoutBuilderController $layout_controller */
    $layout_controller = \Drupal::classResolver()->getInstanceFromDefinition(LayoutBuilderController::class);
    return $layout_controller->layout($section_storage);
  }

  /**
   * {@inheritdoc}
   */
  public function form(FieldItemListInterface $items, array &$form, FormStateInterface $form_state, $get_delta = NULL) {
    $element = parent::form($items, $form, $form_state, $get_delta);
    $form['#entity_builders'][] = [static::class, 'entityBuilder'];
    return $element;
  }

  /**
   * An entity builder to save the layout override field.
   */
  public static function entityBuilder($entity_type, EntityInterface $entity, &$form, FormStateInterface $form_state) {
    // @todo, this isn't resilient to being set twice, during validation and
    // save: https://www.drupal.org/project/drupal/issues/2833682.
    if (!$form_state->isValidationComplete()) {
      return;
    }

    /** @var \Drupal\layout_builder\SectionStorage\SectionStorageManager $section_storage_manager */
    /** @var \Drupal\layout_builder\LayoutTempstoreRepository $temp_store */
    $section_storage_manager = \Drupal::service('plugin.manager.layout_builder.section_storage');
    $temp_store = \Drupal::service('layout_builder.tempstore_repository');

    $section_storage = $section_storage_manager->loadFromStorageId('overrides', sprintf('%s.%s', $entity->getEntityTypeId(), $entity->id()));

    if ($temp_store->has($section_storage)) {
      $temporary_section_storage = $temp_store->get($section_storage);

      // @todo, any way to access this properly?
      $section_list_method = new \ReflectionMethod(OverridesSectionStorage::class, 'getSectionList');
      $section_list_method->setAccessible(TRUE);
      $entity->layout_builder__layout = $section_list_method->invoke($temporary_section_storage);
      $temp_store->delete($temporary_section_storage);
    }
  }

}
