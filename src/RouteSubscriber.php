<?php

namespace Drupal\layout_builder_entity_form;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Add the layout_builder_entity_form routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($collection->getIterator() as $route_id => $route) {
      if (\preg_match('/layout_builder\.overrides\.(?<entity_type_id>[a-z_]+)\.view/', $route_id, $route_information)) {
        $entity_type_id = $route_information['entity_type_id'];
        $route = new Route(sprintf('%s-entity-form', $route->getPath()));
        $route
          ->setDefaults([
            '_entity_form' => "$entity_type_id.layout_form",
            '_title' => 'Edit Layout',
          ])
          ->setRequirement('_entity_access', "$entity_type_id.update")
          ->setOption('parameters', [
            $entity_type_id => ['type' => 'entity:' . $entity_type_id],
          ]);
        $collection->add(sprintf('%s.entity_form', $route_id), $route);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -120];
    return $events;
  }

}
